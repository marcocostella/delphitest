program TestPrj;

uses
  FMX.Forms,
  uMain in 'uMain.pas' {fMain},
  uDM in 'uDM.pas' {DM: TDataModule},
  uEditInsert in 'uEditInsert.pas' {fEditInsert},
  uEIInvoice in 'uEIInvoice.pas' {fEIInvoice},
  uEIInvoiceLine in 'uEIInvoiceLine.pas' {fEIInvoiceLine};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfMain, fMain);
  Application.Run;
end.
