unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.Layouts, FMX.Grid, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Editors, Data.Bind.Components,
  Data.Bind.DBLinks, Fmx.Bind.DBLinks, Data.Bind.DBScope, uDM, FMX.TabControl, FMX.Edit, FMX.Ani, FMX.Objects, FMX.Filter.Effects, FMX.Effects, FMX.Menus,
  Data.Win.ADODB;

type

  TfMain = class(TForm)
    pTop: TPanel;
    tcMain: TTabControl;
    tiInvoices: TTabItem;
    tiCustomers: TTabItem;
    tiProducts: TTabItem;
    eSearch: TEdit;
    lSearch: TLabel;
    imgSearch: TImage;
    imgSearchGlowEffect: TGlowEffect;
    sgAll: TStringGrid;
    TimerSearch: TTimer;
    imgInsert: TImage;
    imgInsertGlowEffect: TGlowEffect;
    imgDelete: TImage;
    imgDeleteGlowEffect: TGlowEffect;
    imgEdit: TImage;
    imgEditGlowEffect: TGlowEffect;
    pmGrid: TPopupMenu;
    mInsert: TMenuItem;
    mEdit: TMenuItem;
    mDelete: TMenuItem;
    imgValInvoice: TImage;
    imgInvValidGlowEffect: TGlowEffect;
    mValInvoice: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure tcMainChange(Sender: TObject);
    procedure imgSearchClick(Sender: TObject);
    procedure TimerSearchTimer(Sender: TObject);
    procedure eSearchChangeTracking(Sender: TObject);
    procedure imgInsertClick(Sender: TObject);
    procedure imgEditClick(Sender: TObject);
    procedure imgDeleteClick(Sender: TObject);
    procedure sgAllDblClick(Sender: TObject);
    procedure sgAllMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure mInsertClick(Sender: TObject);
    procedure mEditClick(Sender: TObject);
    procedure mDeleteClick(Sender: TObject);
    procedure imgValInvoiceClick(Sender: TObject);
    procedure mValInvoiceClick(Sender: TObject);
  private
    { Private declarations }
    FDM: TDM; //data module, one instance for all the application
    FCurrentTable: string; // current table
    procedure InitComponents;
    procedure doOnChangeTab;
    procedure doSearch(ASelId: Integer);
    procedure doSave(AModAction: TModAction);
    function GetCurrentID: Integer;
    procedure doValidateInvoice;
  public
    { Public declarations }
  end;

var
  fMain: TfMain;

implementation

{$R *.fmx}

uses uEditInsert, uEIInvoice;

procedure TfMain.doSave(AModAction: TModAction);
var
  iSavedID,
  iID: Integer;
begin
  iID := -1;
  if (AModAction in [maEdit, maDelete]) then begin
    iID := GetCurrentID;
  end;
  iSavedID := FDM.UIService.Save(AModAction, iID, FCurrentTable, nil);
  // i update always the invoice list, because could be that something has changed in the line
  // in a production framework this will not happen because all the invoice object will be in memory and
  // will be one save for all the object. In this test framework i have one save for the invoie and one for
  // the lines.
  if (iSavedID > -2) or (FCurrentTable = 'invoice') then begin
    doSearch(iSavedID);
  end;
end;

procedure TfMain.doOnChangeTab;
begin
  // to spped up the development of the test application all the tables share some of the UI
  // like the grid, the search and so on... For this reason everytime the Tab change
  // all the shared UI is resetted (this will not happen in a non real application)

  FCurrentTable := '';
  if (tcMain.ActiveTab = tiInvoices) then begin
    FCurrentTable := 'invoice';
  end else if (tcMain.ActiveTab = tiCustomers) then begin
    FCurrentTable := 'customer';
  end else if (tcMain.ActiveTab = tiProducts) then begin
    FCurrentTable := 'product';
  end;

  if (FCurrentTable > '') then begin
    eSearch.Text := '';
    FDM.UIService.FillGrid(FCurrentTable, sgAll, False, '', '', -1);
  end;

  mValInvoice.Visible := (FCurrentTable = 'invoice');
  imgValInvoice.Visible := mValInvoice.Visible;

end;

procedure TfMain.doSearch(ASelId: Integer);
begin
  FDM.UIService.FillGrid(FCurrentTable, sgAll, False, eSearch.Text, '', ASelId);
end;

procedure TfMain.doValidateInvoice;
var
  iJournalId,
  iInvId: Integer;
  oRecInvoice: TBaseRecord;
  eTotCost: Extended;

  procedure InsertJournalLine(AAccount: string; ACredit, ADebit: Extended; AJournalID: Integer);
  begin
    // add the journal line
    FDM.qFactory.Close;
    FDM.qFactory.SQL.Text := 'select * from journal_line where id = -1';
    FDM.qFactory.Open;
    FDM.qFactory.Insert;
    FDM.qFactory.FieldByName('account').AsString := AAccount;
    FDM.qFactory.FieldByName('credit').AsFloat := ACredit;
    FDM.qFactory.FieldByName('debit').AsFloat := ADebit;
    FDM.qFactory.FieldByName('journal_id').AsInteger := AJournalID;
    FDM.qFactory.Post;
  end;

begin
  iInvId := GetCurrentID;

  if (iInvId > -1) then begin

    // invoice data
    oRecInvoice := TBaseRecord.Create(FDM.qFactory, FDM.qJolly, 'invoice', iInvId);

    if (oRecInvoice.FieldByName['status'].Value = 'draft') then begin

      try

        // all the journal data in created in a transaction

        if (FDM.ADOConnection.InTransaction) then
          FDM.ADOConnection.RollbackTrans;
        FDM.ADOConnection.BeginTrans;

        // cal cost
        eTotCost := 0;
        FDM.qJolly.Close;
        FDM.qJolly.SQL.Text := Format('select sum(l.quantity * p.cost)'
                                      + ' from invoice_line l'
                                      + ' left join product p on p.id = l.product_id'
                                      + ' where l.invoice_id = %d'
                                      , [iInvId]);
        FDM.qJolly.Open;
        if not(FDM.qJolly.Eof) then begin
          eTotCost := FDM.qJolly.Fields[0].AsFloat;
        end;
        FDM.qJolly.Close;


        // insert journal
        FDM.qFactory.Close;
        FDM.qFactory.SQL.Text := 'select * from journal where id = -1';
        FDM.qFactory.Open;
        FDM.qFactory.Insert;
        FDM.qFactory.FieldByName('customer_id').AsString := oRecInvoice.FieldByName['customer_id'].Value;
        FDM.qFactory.FieldByName('date').AsDateTime := Now;
        FDM.qFactory.FieldByName('invoice_id').AsInteger := iInvId;
        FDM.qFactory.Post;
        iJournalId := FDM.qFactory.FieldByName('id').AsInteger;

        // insert journal lines
        InsertJournalLine('sale', StrToFloatDef(oRecInvoice.FieldByName['amount'].Value, 0), 0, iJournalId);
        InsertJournalLine('tax', StrToFloatDef(oRecInvoice.FieldByName['tax'].Value, 0), 0, iJournalId);
        InsertJournalLine('ar', 0, StrToFloatDef(oRecInvoice.FieldByName['total'].Value, 0), iJournalId);
        InsertJournalLine('cogs', 0, eTotCost, iJournalId); // debit
        InsertJournalLine('inventory', eTotCost, 0, iJournalId); // credit

        // change the invoice status to validated
        oRecInvoice.FieldByName['status'].Value := 'validated';
        oRecInvoice.Save(maEdit, nil);

        if (FDM.ADOConnection.InTransaction) then
          FDM.ADOConnection.CommitTrans;

         // update the grid with the new data
         doSearch(GetCurrentID);
      finally

        if (FDM.ADOConnection.InTransaction) then
          FDM.ADOConnection.RollbackTrans;

        FreeAndNil(oRecInvoice);

      end;
    end else begin // if (oRecInvoice.FieldByName['status'].Value = 'draft') then begin
      ShowMessage('Only draft invoices could be validated.');
    end;

  end else begin // if (iInvId > -1) then begin
    ShowMessage('No invoice selected.');
  end;
end;

procedure TfMain.eSearchChangeTracking(Sender: TObject);
begin
  // the scope of this code is to reset the timer if someting has changed in the search text
  // the search will  be made only after 0.4sec after the last change
  TimerSearch.Enabled := False;
  TimerSearch.Enabled := True;
end;

procedure TfMain.FormCreate(Sender: TObject);
begin
  InitComponents;
end;

function TfMain.GetCurrentID: Integer;
var
  iRow,
  iCol: Integer;
  oCol: TColumn;
begin

  // get the current ID, is not important what is the current table
  // the framework requires that the primary key of all the table is the
  // field ID (AutoInc)

  Result := -1;
  iRow := sgAll.Selected;

  if (iRow > -1) and (iRow < sgAll.RowCount) then begin

    for iCol := 0 to sgAll.ColumnCount - 1 do begin
      oCol := sgAll.Columns[iCol];
      // the col with tag -111 is the ID col
      if (oCol.Tag = -111) then begin
        Result := StrToIntDef(sgAll.Cells[iCol, iRow], -1);
        Break;
      end;
    end;

  end;

end;

procedure TfMain.imgDeleteClick(Sender: TObject);
begin
  doSave(maDelete);
end;

procedure TfMain.imgEditClick(Sender: TObject);
begin
  doSave(maEdit);
end;

procedure TfMain.imgInsertClick(Sender: TObject);
begin
  doSave(maInsert);
end;

procedure TfMain.imgSearchClick(Sender: TObject);
begin
  doSearch(GetCurrentID);
end;

procedure TfMain.imgValInvoiceClick(Sender: TObject);
begin
  doValidateInvoice;
end;

procedure TfMain.InitComponents;
var
  fTest: TfEditInsert;
  aParam: TFormParams;
begin
  FDM := TDM.GetDataModule;
  tcMain.ActiveTab := tiInvoices;
  doOnChangeTab;
end;

procedure TfMain.mDeleteClick(Sender: TObject);
begin
  doSave(maDelete);
end;

procedure TfMain.mEditClick(Sender: TObject);
begin
  doSave(maEdit);
end;

procedure TfMain.mInsertClick(Sender: TObject);
begin
  doSave(maInsert);
end;

procedure TfMain.mValInvoiceClick(Sender: TObject);
begin
  doValidateInvoice;
end;

procedure TfMain.sgAllDblClick(Sender: TObject);
begin
  doSave(maEdit);
end;

procedure TfMain.sgAllMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var
  iRow: Integer;
  p: TPointF;
begin
  if (ssRight in Shift) then begin

    // select row with right click
    iRow := sgAll.RowByPoint(X, Y);
    if (iRow > -1) then begin
      sgAll.Selected := iRow;
    end;

  end;
end;

procedure TfMain.tcMainChange(Sender: TObject);
begin
  doOnChangeTab;
end;

procedure TfMain.TimerSearchTimer(Sender: TObject);
begin
  TimerSearch.Enabled := False;
  doSearch(GetCurrentID);
end;

end.
