unit uEditInsert;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.Effects, FMX.Objects, uDM,
  FMX.Edit, FMX.ExtCtrls, FMX.ListBox, Data.Win.ADODB;

type
  TfEditInsert = class(TForm)
    pBottom: TPanel;
    pButtons: TPanel;
    StyleBook: TStyleBook;
    imgConfirm: TImage;
    imgOKGlowEffect: TGlowEffect;
    imgCancel: TImage;
    imgCancelGlowEffect: TGlowEffect;
    pLabel: TPanel;
    pEdit: TPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure imgConfirmClick(Sender: TObject);
    procedure imgCancelClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    procedure doConfirm;
    procedure doCancel;
    procedure doResize;
    function doValidate: Boolean;
    procedure UI2Record; // set the UI values to the record
    procedure Record2UI; // set the record values to the UI
  protected
    { Protected declarations }

    // protected variables (visible in the child forms)
    FDM: TDM; //data module, one instance for all the application
    FModAction: TModAction; // the form action insert or edit. delete is not in the
    FMasterRec, // current master record in a master/slave relation
    FBaseRec: TBaseRecord; // current record
    FID: Integer; // current record ID (the same as FBaseRec.FieldByName['id'].Value)
    FTable: string; // current table
    FNoCreateUI: boolean; // do not create the UI

    // virtual, for inherited forms
    function CH_WidthDif: Integer; virtual;
    procedure CH_AfterCreateRecord; virtual;
    procedure CH_AfterSetUI; virtual;
    procedure InitComponents; virtual;
    procedure EditsChange(Sender: TObject); virtual;
    procedure OnCalcFields(ABaseRec: TBaseRecord; AAdoQuery: TAdoQuery); virtual;

  public
    { Public declarations }

    // reintroduce the create of the form to add some mandatory parameters
    constructor Create(AOwner: TComponent; AModAction: TModAction; AParam: TFormParams); reintroduce;

    destructor Destroy; override;
    property BaseRec: TBaseRecord read FBaseRec;
  end;

implementation

{$R *.fmx}

{ TfEditInsert }

procedure TfEditInsert.CH_AfterCreateRecord;
begin
  // do nothing, virtual function
end;

procedure TfEditInsert.CH_AfterSetUI;
begin
  // do nothing, virtual function
end;

function TfEditInsert.CH_WidthDif: Integer;
begin
  // do nothing, virtual function
  Result := 0;
end;

constructor TfEditInsert.Create(AOwner: TComponent; AModAction: TModAction; AParam: TFormParams);
var
  iPointer: LongInt;
begin
  inherited Create(AOwner);
  // set parameters
  FModAction := AModAction;
  FTable := AParam[0];
  FID := StrToIntDef(AParam[1], -1);
  iPointer := StrToIntDef(AParam[2], -1);
  if (iPointer > -1) then begin
    FMasterRec := TBaseRecord(iPointer);
  end;
  InitComponents;
end;

destructor TfEditInsert.Destroy;
begin
  if Assigned(FBaseRec) then begin
    FreeAndNil(FBaseRec);
  end;
  inherited;
end;

procedure TfEditInsert.doCancel;
begin
  Self.Close;
  Self.ModalResult := mrCancel;
end;

procedure TfEditInsert.doConfirm;
begin
  UI2Record;
  if (doValidate) then begin

    if (FBaseRec.Save(FModAction, OnCalcFields) > -2) then begin

      Self.Close;
      Self.ModalResult := mrOk;
    end else begin
      ShowMessage('The data is not saved! Try Again.');
    end;

  end;
end;

procedure TfEditInsert.doResize;
begin
  pButtons.Position.X := (pBottom.Width / 2) - (pButtons.Width / 2);
end;

function TfEditInsert.doValidate: Boolean;
begin
  // there is not validation in this test project
  Result := True;
end;

procedure TfEditInsert.EditsChange(Sender: TObject);
begin
  // dp nothing virtual procedure
end;

procedure TfEditInsert.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // on close the form is hidden, will be destroyed in the same place that is created
  // i need an alive form because i have to check the modalresult after the form is closed
  Action := TCloseAction.caHide;
end;

procedure TfEditInsert.FormResize(Sender: TObject);
begin
  doResize;
end;

procedure TfEditInsert.imgCancelClick(Sender: TObject);
begin
  doCancel;
end;

procedure TfEditInsert.imgConfirmClick(Sender: TObject);
begin
  doConfirm;
end;

procedure TfEditInsert.InitComponents;
var
  sTmp: string;
begin
  FDM := TDM.GetDataModule;

  // recalc position of some UI components
//  doResize;

  // update the form caption
  Self.ModalResult := mrCancel;
  case FModAction of
    maEdit: begin
      sTmp := 'Edit: ' + FTable + Format(' (id=%d)', [FID]);
    end;
    maInsert: begin
      sTmp := 'Insert: ' + FTable;
    end;
  end;
  Self.Caption := sTmp;

  // load the record
  FBaseRec := TBaseRecord.Create(FDM.qFactory, FDM.qJolly, FTable, FID);
  // call the method that will be developed in the child forms
  CH_AfterCreateRecord;

  // create the fields link RECORD -> UI;
  if not(FNoCreateUI) then begin
    Record2UI;
  end;

end;

procedure TfEditInsert.OnCalcFields(ABaseRec: TBaseRecord; AAdoQuery: TAdoQuery);
begin
  // do nothing, virtual function
end;

procedure TfEditInsert.Record2UI;
var
  eTop: Single;
  oLabel: TLabel;
  oEdit: TCustomEdit;
  oField: TBaseField;
  iComboIndex,
  iComboVal,
  iCount: Integer;
begin
  eTop := 20;
  for iCount := 0 to FBaseRec.FieldsCount - 1 do begin
    oField := FBaseRec.Fields[iCount];

    if not(oField.FieldType in [bstUnknown, bstID]) then begin

      // create the label
      oLabel := TLabel.Create(Self);
      oLabel.Parent := pLabel;
      oLabel.Text := oField.Name;
      oLabel.Name := 'l' + oField.Name;
      oLabel.Position.Y := eTop;
      oLabel.Align := TAlignLayout.alHorizontal;
      oLabel.Padding.Right := 10;
      oLabel.TextAlign := TTextAlign.taTrailing;

      // create the edit
      oEdit := nil;
      case oField.FieldType of
        bstStr: begin
          oEdit := TEdit.Create(Self);
        end;
        bstNumber: begin
          oEdit := TNumberBox.Create(Self);
          (oEdit as TNumberBox).Max := 99999999;
          (oEdit as TNumberBox).Min := -99999999;
          (oEdit as TNumberBox).ValueType := TNumValueType.vtFloat;
          (oEdit as TNumberBox).VertIncrement := 1;
        end;
        bstFK: begin
          oEdit := TComboEdit.Create(Self);
          iComboIndex := -1;
          for iComboVal := 0 to Length(oField.ComboVals) - 1 do begin
            (oEdit as TComboEdit).Items.AddObject(oField.ComboVals[iComboVal].Value, oField.ComboVals[iComboVal]);
            if (IntToStr(oField.ComboVals[iComboVal].Key) = oField.Value) then begin
              iComboIndex := iComboVal;
            end;
          end;
          (oEdit as TComboEdit).ItemIndex := iComboIndex;
        end;
        bstDateTime: begin
          oEdit := TCalendarEdit.Create(Self);
        end;
      end;
      if Assigned(oEdit) then begin
        oEdit.Parent := pEdit;
        if not(oEdit is TComboEdit) then begin
          oEdit.Text := oField.Value;
        end;
        oEdit.Name := 'e' + oField.Name;
        oEdit.Position.Y := eTop;
        oEdit.Align := TAlignLayout.alHorizontal;
        oEdit.Padding.Right := 10;
        oEdit.OnChange := EditsChange;
      end;

      oField.UI := oEdit;

      eTop := eTop + 32;
    end;

  end;
  // resize the form based on the number of the fields
  Self.Height := Trunc(eTop + pBottom.Height + 60) + CH_WidthDif;
  // call the method that will be developed in the child forms
  CH_AfterSetUI;
end;

procedure TfEditInsert.UI2Record;
var
  oField: TBaseField;
  iKey,
  iCount: Integer;
begin
  for iCount := 0 to FBaseRec.FieldsCount - 1 do begin
    oField := FBaseRec.Fields[iCount];
    if Assigned(oField.UI) then begin
      if (oField.UI is TComboEdit) then begin
        iKey := oField.GetComboKey;
        if (iKey > -1) then begin
          oField.Value := IntToStr(iKey);
        end;
      end else begin
        oField.Value := oField.UI.Text;
      end;
    end;
  end;
end;

end.
