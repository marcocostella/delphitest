unit uDM;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Data.Win.ADODB, Data.DBXOdbc, Data.SqlExpr,
  FMX.Dialogs, FMX.Grid, FMX.Types, FMX.Edit, FMX.ExtCtrls, FMX.Forms, System.UITypes;

type


  // note:
  // all the db query objects of this test application are in the DM
  // but in a real application will be better develop a POOL service

  // combobox values
  TComboVal<TKey, TValue> = class
  private
    FKey: TKey;
    FValue: TValue;
    procedure SetKey(const Value: TKey);
    procedure SetValue(const Value: TValue);
  public
    property Key: TKey read FKey write SetKey;
    property Value: TValue read FValue write SetValue;
  end;
  TNComboVal = TComboVal<Integer, string>;
  TNComboVals = TArray<TNComboVal>;

  TBaseRecord = class;

  // this is needed when a row is inserted or modified (UI)
  TModAction = (maEdit, maInsert, maDelete);

  // edit/insert form parameters
  TFormParams = array of string;

  // on calc field event
  TOnCalcField = procedure(ABaseRec: TBaseRecord; AQUery: TAdoQuery) of object;

  // Field Types
  TBaseFieldType = (bstUnknown, bstStr, bstNumber, bstID, bstFK, bstDateTime);

  // Base field
  TBaseField = class(TObject)
  private
    FName: string; // name of the database field
    FFieldType: TBaseFieldType; // type of the field
    FValue: string; // value, all the values are saved as string, and eventually converted
    FOrigValue: string; // origin value, the value before modification. not used in this framework but gives an idea of a good framework (where only the modify data is saved)
    FUI: TCustomEdit; // UI control of the field (assigned in the TfEditInsert.Record2UI method)
    FComboVals: TNComboVals; // values in case of combo box
    procedure SetName(const Value: string);
    procedure SetFieldType(const Value: TBaseFieldType);
    procedure SetValue(const Value: string);
    procedure SetUI(const Value: TCustomEdit);
    procedure ClearComboVals; // clear the combo values
  public
    constructor Create;
    destructor Destroy; override;
    property Name: string read FName write SetName;
    property FieldType: TBaseFieldType read FFieldType write SetFieldType;
    property Value: string read FValue write SetValue;
    property OrigValue: string read FOrigValue;
    property UI: TCustomEdit read FUI write SetUI;
    property ComboVals: TNComboVals read FComboVals;
    procedure SetLenghtComboVals(ALen: Integer); // set the count of the combo items
    procedure ResetDirty; // reset the field as no dirty (see FOrigValue for info)
    function IsDirty: Boolean; // is the field dirty? (see FOrigValue for info)
    function GetComboKey: Integer; // get the current value of the related combo box

    // maybe was better write an AsExtended property...
    function GetExtendedVal: Extended; // get the value (the is always string) in Extended type.
    procedure SetExtendedVal(AValue: Extended); // the the value as extended
  end;

  // Base Record (collection of fields)
  TBaseRecord = class(TObject)
  private
    FFields: TStringList; // fields
    FQueryJolly, // ado query
    FQuery: TADOQuery; // ado query
    FTableName: string; // database table name
    FID: Integer; // ID ( see TfMain.GetCurrentID for more info )
    function GetFields(Index: Integer): TBaseField;
    function GetFieldByName(Name: string): TBaseField;
  public
    constructor Create(AQuery, AQueryJolly: TADOQuery; ATable: string; AID: Integer);
    destructor Destroy; override;
    procedure AddField(AField: TField; AAssignValues: Boolean);
    procedure ClearFields;
    property Fields[Index: Integer]: TBaseField read GetFields;
    property FieldByName[Name: string]: TBaseField read GetFieldByName;
    function FieldsCount: Integer;
    function Save(AModAction: TModAction; AOnCalcFields: TOnCalcField): Integer; // save the record (insert, update or delete)
  end;

  // The UIService is in charge of some UI operation
  TUIService = class(TObject)
  private
    FQueryJolly,
    FQuery: TADOQuery;
    procedure RaiseNoGrid;
  public
    constructor Create(AQuery, AQueryJolly: TADOQuery);

    // fill a string grid. i use this as TBaseRecord collection (but there is no TBaseRecord in the stringgrid) because this is a test
    // framework, otherwise will be better to develop a TBaseTable
    procedure FillGrid(ATableName: string; AGrid: TStringGrid; ANoFill: Boolean; AFilter: string; ASQLWhere: string; AIDToSel: Integer);

    procedure ClearGrid(AGrid: TStringGrid); // clear the grid
    function ColumnByName(AGrid: TStringGrid; AColumnName: string): TStringColumn; // get a grid column by the database name

    // this save is the UI part of the TBaseRecord.Save
    function Save(AModAction: TModAction; AID: Integer; ATAble: string; AMasterRecord: TBaseRecord): Integer;

    procedure SetFieldReadOnly(ABField: TBaseField); // set the fields as readonly
  end;

  // In this framework i dont need record lists (tables)

  // The DATA MODULE
  TDM = class(TDataModule)
    ADOConnection: TADOConnection;  // ado connection
    qJolly: TADOQuery; // query
    qFactory: TADOQuery; // query
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FAppPath: string; // application path. i need it to create the connection string to the database
    FUIService: TUIService; // ui services
    procedure InitComponents;
  public
    { Public declarations }
    class function GetDataModule: TDM; //data module, one instance for all the application
    property AppPath: string read FAppPath;
    property UIService: TUIService read FUIService;
  end;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

uses uEditInsert, uEIInvoice, uEIInvoiceLine;

var
  FDataModule: TDM;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  InitComponents;
end;

procedure TDM.DataModuleDestroy(Sender: TObject);
begin
  ADOConnection.Connected := False;
  FreeAndNil(FUIService);
end;

class function TDM.GetDataModule: TDM;
begin
  if not(Assigned(FDataModule)) then begin
    FDataModule := TDM.Create(nil);
  end;
  Result := FDataModule;
end;

procedure TDM.InitComponents;
begin
  // app path
  FAppPath := ExtractFileDir(ParamStr(0));

  // due to excel driver limitation I switched to access: https://support.microsoft.com/en-us/kb/178717
  // open database connection
  ADOConnection.Connected := False;
  ADOConnection.ConnectionString := Format('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=%s\db.mdb;Persist Security Info=False', [FAppPath]);
  ADOConnection.Connected := True;

  // set some base sercives
  FUIService := TUIService.Create(qFactory, qJolly);
end;

{ TBaseField }


procedure TBaseField.ClearComboVals;
var
  iCount: Integer;
  oComboVal: TNComboVal;
begin
  for iCount := 0 to Length(ComboVals) - 1 do begin
    oComboVal := ComboVals[iCount];
    FreeAndNil(oComboVal);
    ComboVals[iCount] := nil;
  end;
end;

constructor TBaseField.Create;
begin
  FName := '';
  FFieldType := bstUnknown;
  FValue := '';
  FOrigValue := '';
  FUI := nil;
  SetLenghtComboVals(0);
end;

destructor TBaseField.Destroy;
begin
  ClearComboVals;
  inherited;
end;

function TBaseField.GetComboKey: Integer;
var
  oCEdit: TComboEdit;
  iIndex: Integer;
  oNVal: TNComboVal;
begin
  Result := -1;
  if Assigned(UI) and (UI is TComboEdit) then begin
    oCEdit := (UI as TComboEdit);
    iIndex := oCEdit.ItemIndex;
    if (iIndex > -1) then begin
      oNVal := (oCEdit.Items.Objects[iIndex] as TNComboVal);
      Result := oNVal.Key;
    end;
  end;
end;

function TBaseField.GetExtendedVal: Extended;
var
  oNEdit: TNumberBox;
  iIndex: Integer;
  oNVal: TNComboVal;
begin
  Result := 0;
  if Assigned(UI) and (UI is TNumberBox) then begin
    oNEdit := (UI as TNumberBox);
    Result := oNEdit.Value;
  end;
end;

function TBaseField.IsDirty: Boolean;
begin
  Result := (FOrigValue <> Value);
end;

procedure TBaseField.ResetDirty;
begin
  FOrigValue := Value;
end;

procedure TBaseField.SetExtendedVal(AValue: Extended);
var
  oNEdit: TNumberBox;
  iIndex: Integer;
  oNVal: TNComboVal;
begin
  if Assigned(UI) and (UI is TNumberBox) then begin
    oNEdit := (UI as TNumberBox);
    oNEdit.Value := AValue;
  end;
end;

procedure TBaseField.SetFieldType(const Value: TBaseFieldType);
begin
  FFieldType := Value;
end;


procedure TBaseField.SetLenghtComboVals(ALen: Integer);
var
  iCount: Integer;
begin
  ClearComboVals;
  SetLength(FComboVals, ALen);
  for iCount := 0 to ALen - 1 do begin
    ComboVals[iCount] := TNComboVal.Create;
  end;
end;

procedure TBaseField.SetName(const Value: string);
begin
  FName := Value;
end;


procedure TBaseField.SetUI(const Value: TCustomEdit);
begin
  FUI := Value;
end;

procedure TBaseField.SetValue(const Value: string);
begin
  FValue := Value;
end;

{ TUIService }

procedure TUIService.ClearGrid(AGrid: TStringGrid);
var
  iCount: Integer;
  oCol: TColumn;
begin
  if Assigned(AGrid) then begin
    AGrid.RowCount := 0;
    for iCount := AGrid.ColumnCount - 1 downto 0 do begin
      oCol := AGrid.Columns[iCount];
      FreeAndNil(oCol);
    end;
  end else begin
    RaiseNoGrid;
  end;
end;

function TUIService.ColumnByName(AGrid: TStringGrid; AColumnName: string): TStringColumn;
var
  iCount: Integer;
begin
  Result := nil;
  if Assigned(AGrid) then begin
    for iCount := 0 to AGrid.ColumnCount - 1 do begin
      if (AGrid.Columns[iCount].Name = AColumnName) then begin
        Result := TStringColumn(AGrid.Columns[iCount]);
        Break;
      end;
    end;
  end else begin
    RaiseNoGrid;
  end;
end;

constructor TUIService.Create(AQuery, AQueryJolly: TADOQuery);
begin
  FQuery := AQuery;
  FQueryJolly := AQueryJolly;
  if not(Assigned(FQuery)) or not(Assigned(FQueryJolly)) then begin
    raise Exception.Create('Programming Error: The UIService needs 2 DB Query to operate.');
  end;
end;

procedure TUIService.FillGrid(ATableName: string; AGrid: TStringGrid; ANoFill: Boolean; AFilter: string; ASQLWhere: string; AIDToSel: Integer);
var
  oBR: TBaseRecord;
  iSelRow,
  iRow,
  iCount: Integer;
  oField: TBaseField;
  oCol: TStringColumn;
  oDBField: TField;
  bAddRow: Boolean;
begin
  if Assigned(AGrid) then begin

    iSelRow := -1;
    ClearGrid(AGrid);

    // create an empty record (only structure)
    oBR := TBaseRecord.Create(FQuery, FQueryJolly, ATableName, -1);
    try
      // fill the grid columns
      for iCount := 0 to oBR.FieldsCount - 1 do begin
        oField := oBR.Fields[iCount];
        oCol := TStringColumn.Create(AGrid);
        oCol.Parent := AGrid;
        oCol.Header := oField.Name;
        oCol.Name := oField.Name;
        case oField.FieldType of
          bstStr: oCol.Width := 240;
          else oCol.Width := 120;
        end;
        if (oField.FieldType in [bstID]) then begin
          oCol.Visible := False;
          oCol.Tag := -111; // the columns with Tag -111 are ID columns
        end;

      end;
    finally
      FreeAndNil(oBR);
    end;

    try
      AGrid.BeginUpdate;

      // load the data and create the grid rows
      // in this case i don't use a record, because i didn't developed a record list
      if not(ANoFill) then begin

        // open the dataset with all the rows
        // to make it more easy i use an hardcoded where to filter the related rows
        // in a production framework this will be calculated with the model info
        FQuery.Close;
        FQuery.SQL.Text := Format('select * from %s %s', [ATableName, ASQLWhere]);
        FQuery.Open;
        while not(FQuery.Eof) do begin
          // add the row
          AGrid.RowCount := AGrid.RowCount + 1;
          iRow := AGrid.RowCount - 1;

          // set the grid cells anc check if the row is in the current filter
          bAddRow := False;
          for iCount := 0 to FQuery.FieldCount - 1 do begin
            oDBField := FQuery.Fields[iCount];
            oCol := ColumnByName(AGrid, oDBField.FullName);
            if Assigned(oCol) then begin
              AGrid.Cells[oCol.Index, iRow] := oDBField.AsString;
            end;
            bAddRow := (bAddRow) or (AFilter = '') or (Pos(AFilter, oDBField.AsString) > 0);
          end;

          // if the row is not to add i delete it
          // in a non test framework the filter will be made in the database with the select
          if not(bAddRow) then begin
            AGrid.RowCount := AGrid.RowCount - 1;
          end;

          if (FQuery.FieldByName('id').AsInteger = AIDToSel) then begin
            iSelRow := AGrid.RowCount - 1;
          end;

          FQuery.Next;
        end;
        FQuery.Close;

      end;

      // select the current row
      if (iSelRow > -1) then begin
        AGrid.Selected := iSelRow;
      end;


    finally
      AGrid.EndUpdate;
    end;

  end else begin // if Assigned(AGrid) then begin
    RaiseNoGrid;
  end;
end;

procedure TUIService.RaiseNoGrid;
begin
  raise Exception.Create('Programming Error: The grid does not exists.');
end;

function TUIService.Save(AModAction: TModAction; AID: Integer; ATAble: string; AMasterRecord: TBaseRecord): Integer;
var
  aParam: TFormParams;
  fEditInsert: TfEditInsert;
  oRec: TBaseRecord;
begin
  // the rusult could be:
  // -2 if there is nothing to save or save errors
  // -1 if the record is deleted
  // > -1 = record ID: the record ID saved or edited

  // checks
  Result := -2;
  if (AModAction in [maEdit, maDelete]) then begin
    if (AID < 0) then begin
      if (AModAction = maEdit) then
        ShowMessage('Nothing to edit')
      else
        ShowMessage('Nothing to delete');
      Exit;
    end;
  end;

  // questions
  if (AModAction = maDelete) and ((MessageDlg('Delete selected row?', TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0) <> 6)) then begin
    Exit;
  end;

  // in case of edit or insert a for is dinamically creaded and filled with UI and values
  if (AModAction in [maEdit, maInsert]) then begin

    // parameters
    // 0 = db table name
    // 1 = current ID
    // 2 = pointer to master record (here is usually the current record)
    SetLength(aParam, 3);
    aParam[0] := ATAble ;
    aParam[1] := IntToStr(AID);
    if Assigned(AMasterRecord) then begin
      aParam[2] := IntToStr(LongInt(AMasterRecord));
    end else begin
      aParam[2] := '';
    end;

    // different forms for different tables
    // note that:
    // customer and product are simple forms and is enought the TfEditInsert
    // invoice and invoice_line are more complex forms
    // for journal and journal lines there is no forms: in this test are readonly
    if (ATAble = 'invoice') then begin
      fEditInsert := TfEIInvoice.Create(nil, AModAction, aParam);
    end else if (ATAble = 'invoice_line') then begin
      fEditInsert := TfEIInvoiceLine.Create(nil, AModAction, aParam);
    end else begin
      fEditInsert := TfEditInsert.Create(nil, AModAction, aParam);
    end;

    try
      // show the form
      fEditInsert.ShowModal;
      if (fEditInsert.ModalResult = mrOK) then begin
        // return the saved id
        Result := fEditInsert.BaseRec.FID;
      end;

    finally
      fEditInsert.Release;
      fEditInsert := nil;
    end;

  // delete the record, in this case i don't need a form
  end else begin
    oRec := TBaseRecord.Create(FQuery, FQueryJolly, ATAble, AID);
    try
      Result := oRec.Save(AModAction, nil);
    finally
      FreeAndNil(oRec);
    end;
  end;
end;

procedure TUIService.SetFieldReadOnly(ABField: TBaseField);
begin
  if Assigned(ABField) then begin
    ABField.UI.Enabled := False;
    ABField.UI.ReadOnly := True;
    ABField.UI.HitTest := False;
  end;
end;

{ TBaseRecord }

procedure TBaseRecord.AddField(AField: TField; AAssignValues: Boolean);
var
  oBField: TBaseField;

  procedure SetComboValues(ASelect: string);
  var
    iCount: Integer;
  begin
    // i use the QueryJolly, bacause other query is open at this point
    FQueryJolly.Close;
    FQueryJolly.SQL.Text := ASelect;
    FQueryJolly.Open;

    // problem with buffer, driver and ado
    FQueryJolly.Last;
    FQueryJolly.First;

    oBField.SetLenghtComboVals(FQueryJolly.RecordCount);
    iCount := 0;
    while not(FQueryJolly.Eof) do begin
      oBField.ComboVals[iCount].Key := FQueryJolly.Fields[0].AsInteger;
      oBField.ComboVals[iCount].Value := FQueryJolly.Fields[1].AsString;
      FQueryJolly.Next;
      iCount := iCount + 1;
    end;
  end;

begin
  if Assigned(AField) then begin
    oBField := TBaseField.Create;
    FFields.AddObject(AField.FullName, oBField);
    oBField.Name := AField.FullName;
    // this is a dummy logic for the test
    // in a real application will be better get the info from the system tables
    case AField.DataType of
      ftAutoInc: oBField.FieldType := bstID;
      ftWideString: oBField.FieldType := bstStr;
      ftBCD, ftFMTBcd: oBField.FieldType := bstNumber;
      ftInteger: oBField.FieldType := bstFK;
      ftDateTime: oBField.FieldType := bstDateTime;
      else oBField.FieldType := bstUnknown;
    end;

    if (AAssignValues) then begin
      oBField.Value := Trim(AField.AsString);


    // default value
    end else begin
      // default values, this is one of the features that in this test are hardcoded
      // but in a production framework will be saved in the model info
      if (oBField.Value = '') then begin
        case AField.DataType of
          ftBCD, ftFMTBcd: oBField.Value := '0';
          ftDateTime: oBField.Value := DateToStr(Now);
        end
      end;
      oBField.ResetDirty;
    end;

    // this is another feature that will be better to save in the model info
    if (oBField.Name = 'customer_id') then begin
      SetComboValues('select id, trim(name) from customer order by name;');
    end else if (oBField.Name = 'product_id') then begin
      SetComboValues('select id, trim(name) from product order by name;');
    end;

  end else begin
    raise Exception.Create('Programming Error: There is no DB field to add.');
  end;
end;

procedure TBaseRecord.ClearFields;
var
  iCount: Integer;
  oBField: TBaseField;
begin
  // clear the fields
  for iCount := FFields.Count - 1 downto 0 do begin
    oBField := (FFields.Objects[iCount] as TBaseField);
    FreeAndNil(oBField);
    FFields.Objects[iCount] := nil;
  end;
  FFields.Clear;
end;

constructor TBaseRecord.Create(AQuery, AQueryJolly: TADOQuery; ATable: string; AID: Integer);
var
  bAssignValues: Boolean;
  iCount: Integer;
  oField: TField;
begin
  inherited Create;

  FFields := TStringList.Create;
  FQuery := AQuery;
  FQueryJolly := AQueryJolly;
  FTableName := ATable;
  FID := AID;

  try
    // load the data
    FQuery.Close;
    FQuery.SQL.Text := Format('select * from %s where id = %d;', [FTableName, FID]);
    FQuery.Open;

    // sometimes with ADO the RecordCount is not updated
    // usually because the driver use a buffer to page the recordset and the record count correspond to the page size
    // to avoid errors i need to move the reoordset to the last record and then back to the first one
    FQuery.Last;
    FQuery.First;

    if (FQuery.RecordCount > 1) then begin
      raise Exception.Create('Too many record to load');
    end;

    // the record structure is alweys created, the data is loaded only if there is data to load
    bAssignValues := not(FQuery.Eof);

    // create the record
    // in a real application the db structure could be saved in an object to avoid
    // the structure creation every time a record is loaded
    for iCount := 0 to FQuery.FieldCount - 1 do begin
      oField := FQuery.Fields.Fields[iCount];
      Self.AddField(oField, bAssignValues);
    end;

  except
    on E:Exception do begin
      ShowMessage('Programming Error: An error occurred in the Create Record method' + #13#10 + E.Message);
    end;
  end;
  FQuery.Close;

end;

destructor TBaseRecord.Destroy;
begin
  // free all the allocated objects
  Self.ClearFields;
  FreeAndNil(FFields);
  inherited;
end;

function TBaseRecord.FieldsCount: Integer;
begin
  Result := FFields.Count;
end;

function TBaseRecord.GetFieldByName(Name: string): TBaseField;
var
  iIndex: Integer;
begin
  iIndex := FFields.IndexOf(Name);
  Result := Fields[iIndex];
end;

function TBaseRecord.GetFields(Index: Integer): TBaseField;
begin
  Result := nil;
  if (Index >= 0) and (Index < FFields.Count) then begin
    Result := TBaseField(FFields.Objects[Index]);
  end;
end;



function TBaseRecord.Save(AModAction: TModAction; AOnCalcFields: TOnCalcField): Integer;
var
  iCount: Integer;
  oField: TBaseField;
  oDBField: TField;
begin
  Result := -2; // no save
  try

    // open a query and set the selected action
    FQuery.Close;
    FQuery.SQL.Text := Format('select * from %s where id = %d;', [FTableName, FID]);
    FQUery.Open;
    case AModAction of
      maEdit: FQuery.Edit;
      maInsert: FQuery.Insert;
      maDelete: FQuery.Delete;
    end;

    // if the action is not delete i need to set the query fields values
    if (AModAction <> maDelete) then begin
      for iCount := 0 to FieldsCount - 1 do begin
        oField := Fields[iCount];
        oDBField := FQuery.FieldByName(oField.Name);
        if Assigned(oDBField) and not(oField.FieldType in [bstID, bstUnknown]) then begin
          if (oField.FieldType <> bstFK) or (oField.Value > '') then
            oDBField.Value := oField.Value;
        end;
      end;
    end;

    // update calculated fields and save the changes
    if (AModAction in [maEdit, maInsert]) then begin
      if Assigned(AOnCalcFields) then begin
        AOnCalcFields(Self, FQuery);
      end;
      FQuery.Post;
    end;

    case AModAction of
      maEdit: Result := FID; // id of the edited record
      maInsert: begin
        // id of the inserted record
        Self.FieldByName['id'].Value := FQuery.FieldByName('id').AsString;
        FID := FQuery.FieldByName('id').AsInteger;
        Result := FID;
      end;
      maDelete: Result := -1; // record deleted
    end;
  except
    on E:Exception do begin
      ShowMessage('Programming Error: An error occurred in the Record Save' + #13#10 + E.Message);
    end;
  end;
end;

{ TComboVal<TKey, TValue> }

procedure TComboVal<TKey, TValue>.SetKey(const Value: TKey);
begin
  FKey := Value;
end;

procedure TComboVal<TKey, TValue>.SetValue(const Value: TValue);
begin
  FValue := Value;
end;

initialization
  FDataModule := nil;

finalization
  if Assigned(FDataModule) then begin
    FreeAndNil(FDataModule);
  end;

end.
