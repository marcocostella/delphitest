unit uEIInvoice;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, uEditInsert, FMX.Effects,
  FMX.Objects, FMX.TabControl, FMX.Layouts, FMX.Grid, uDM, Data.Win.ADODB, FMX.Edit, FMX.ExtCtrls;

type
  // inherited from the EditInsert FORM
  TfEIInvoice = class(TfEditInsert)
    pDetail: TPanel;
    pDetailTop: TPanel;
    tcInvoice: TTabControl;
    tiLine: TTabItem;
    tiJournal: TTabItem;
    sgInvDet: TStringGrid;
    pInvDetail: TPanel;
    pInvDetailBottom: TPanel;
    pInvDetailTop: TPanel;
    imgInvDetDelete: TImage;
    imgDeleteGlowEffect: TGlowEffect;
    imgInvDetEdit: TImage;
    imgEditGlowEffect: TGlowEffect;
    imgInvDetInsert: TImage;
    imgInsertGlowEffect: TGlowEffect;
    lInvLIneInfo: TLabel;
    lJournalData: TLabel;
    eJournalData: TCalendarEdit;
    procedure tcInvoiceChange(Sender: TObject);
    procedure imgInvDetInsertClick(Sender: TObject);
    procedure imgInvDetEditClick(Sender: TObject);
    procedure imgInvDetDeleteClick(Sender: TObject);
    procedure sgInvDetDblClick(Sender: TObject);
  private
    { Private declarations }
    FCurrentDetailTable: string; // current detail table, the invoice has 2 detail tables lines and journal + journal line
    procedure doOnChangeTab;
    procedure doSaveInvLine(AModAction: TModAction);
    function GetCurrentDetID: Integer;
    procedure doSearchLines(ASelID: Integer);
    procedure UpdateInvTotals;
    procedure DisableCustomer;
  protected
    { Protected declarations }
    function CH_WidthDif: Integer; override;
    procedure CH_AfterSetUI; override;
    procedure CH_AfterCreateRecord; override;
    procedure InitComponents; override;
    procedure OnCalcFields(ABaseRec: TBaseRecord; AAdoQuery: TAdoQuery); override;
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

{ TfEIInvoice }

procedure TfEIInvoice.CH_AfterCreateRecord;
begin
  inherited;
  // set default data
  if (FModAction = maInsert) then begin
   FBaseRec.FieldByName['status'].Value := 'draft';
  end;
end;

procedure TfEIInvoice.CH_AfterSetUI;
begin
  inherited;

  // in a production framework all this UI logic will be saved in
  // the model indfo

  // changes to the invoice fields
  FDM.UIService.SetFieldReadOnly(FBaseRec.FieldByName['amount']);
  FDM.UIService.SetFieldReadOnly(FBaseRec.FieldByName['tax']);
  FDM.UIService.SetFieldReadOnly(FBaseRec.FieldByName['total']);
  FDM.UIService.SetFieldReadOnly(FBaseRec.FieldByName['status']);

  imgInvDetInsert.Visible := (FModAction <> maInsert);
  imgInvDetEdit.Visible := imgInvDetInsert.Visible;
  imgInvDetDelete.Visible := imgInvDetInsert.Visible;

  lInvLIneInfo.Visible := not(imgInvDetInsert.Visible);

  // if validated, no changes
  if (FBaseRec.FieldByName['status'].Value <> 'draft') then begin
    pEdit.Enabled := False;
    imgInvDetInsert.Visible := False;
    imgInvDetDelete.Visible := False;
    imgConfirm.Visible := False;
  end;

  // set the journal data
  eJournalData.Text := '';
  FDM.qJolly.Close;
  FDM.qJolly.SQL.Text := Format('select date from journal where invoice_id = %d', [FID]);
  FDM.qJolly.Open;
  if not(FDM.qJolly.Eof) then begin
    eJournalData.Date := FDM.qJolly.Fields[0].AsDateTime;
  end;
  FDM.qJolly.Close;

  DisableCustomer;
end;

function TfEIInvoice.CH_WidthDif: Integer;
begin
  Result := Trunc(pDetail.Height);
end;


procedure TfEIInvoice.DisableCustomer;
var
  iCount: Integer;
begin
  // the customer could be changed only when there is no rows
  // otherwise i need to recalculate all the taxes for all the row and total
  // ... and this is a test application...
  iCount := 0;
  FDM.qJolly.Close;
  FDM.qJolly.SQL.Text := Format('select count(*) from invoice_line where invoice_id = %d', [FID]);
  FDM.qJolly.Open;
  if not(FDM.qJolly.Eof) then begin
    iCount := FDM.qJolly.Fields[0].AsInteger;
  end;
  FDM.qJolly.Close;

  FBaseRec.FieldByName['customer_id'].UI.Enabled := (iCount = 0);
  FBaseRec.FieldByName['customer_id'].UI.ReadOnly := (iCount <> 0);
end;

procedure TfEIInvoice.doOnChangeTab;
begin
  // this UI logic is similar to the one in the main form
  // in a production framework will be better to develop one system
  // that will cover all this kind of UI

  // here i didn't do that system because i want to show you
  // how a form could be inherited from another

  FCurrentDetailTable := '';
  if (tcInvoice.ActiveTab = tiLine) then begin
    FCurrentDetailTable := 'invoice_line';
    pInvDetailTop.Visible := True;
    pInvDetailBottom.Visible := False;
    pInvDetail.Height := pInvDetailTop.Height;
  end else if (tcInvoice.ActiveTab = tiJournal) then begin
    FCurrentDetailTable := 'journal_line';
    pInvDetailTop.Visible := False;
    pInvDetailBottom.Visible := True;
    pInvDetail.Height := pInvDetailBottom.Height;
  end;

  if (FCurrentDetailTable > '') then begin
    doSearchLines(-1);
  end;
end;

procedure TfEIInvoice.doSaveInvLine(AModAction: TModAction);
var
  iSavedId,
  iDetID: Integer;
begin
  // edit, insert, delete invoice line
  iDetID := -1;
  if (AModAction in [maEdit, maDelete]) then begin
    iDetID := GetCurrentDetID;
  end;
  iSavedId := FDM.UIService.Save(AModAction, iDetID, FCurrentDetailTable, FBaseRec);
  if (iSavedId > -2) then begin
    doSearchLines(iSavedId);
    UpdateInvTotals;
    DisableCustomer;
  end;
end;

procedure TfEIInvoice.doSearchLines(ASelID: Integer);
var
  sWhere: string;
begin
  // fill the detail grid
  if (FCurrentDetailTable = 'invoice_line') then begin
    // invoice lines
    sWhere := Format('where invoice_id = %d', [FID]);
  end else begin
    // journal lines
    sWhere := Format('where journal_id in (select id from journal where invoice_id = %d)', [FID]);
  end;
  FDM.UIService.FillGrid(FCurrentDetailTable, sgInvDet, False, '', sWhere, ASelID);
end;

function TfEIInvoice.GetCurrentDetID: Integer;
var
  iRow,
  iCol: Integer;
  oCol: TColumn;
begin
  // the same as TfMain.GetCurrentID
  // in a production framework the grid layout will be manage as an edit

  Result := -1;
  iRow := sgInvDet.Selected;
  if (iRow > -1) and (iRow < sgInvDet.RowCount) then begin
    for iCol := 0 to sgInvDet.ColumnCount - 1 do begin
      oCol := sgInvDet.Columns[iCol];
      // the col with tag -111 is the ID col
      if (oCol.Tag = -111) then begin
        Result := StrToIntDef(sgInvDet.Cells[iCol, iRow], -1);
        Break;
      end;
    end;
  end;
end;

procedure TfEIInvoice.imgInvDetDeleteClick(Sender: TObject);
begin
  inherited;
  doSaveInvLine(maDelete);
end;

procedure TfEIInvoice.imgInvDetEditClick(Sender: TObject);
begin
  inherited;
  doSaveInvLine(maEdit);
end;

procedure TfEIInvoice.imgInvDetInsertClick(Sender: TObject);
begin
  inherited;
  doSaveInvLine(maInsert);
end;

procedure TfEIInvoice.InitComponents;
begin
  inherited InitComponents;
  FCurrentDetailTable := '';
  tcInvoice.ActiveTab := tiLine;
  doOnChangeTab;
end;

procedure TfEIInvoice.OnCalcFields(ABaseRec: TBaseRecord; AAdoQuery: TAdoQuery);
begin
  inherited;
end;

procedure TfEIInvoice.sgInvDetDblClick(Sender: TObject);
begin
  inherited;
  if (FCurrentDetailTable = 'invoice_line') then begin
    doSaveInvLine(maEdit);
  end;
end;

procedure TfEIInvoice.tcInvoiceChange(Sender: TObject);
begin
  inherited;
  doOnChangeTab;
end;

procedure TfEIInvoice.UpdateInvTotals;
var
  eTotal,
  eTax,
  eAmount: Extended;
begin
  // update the total of the invoice when something is changing in the lines or in the invoice
  eTotal := 0;
  eTax := 0;
  eAmount := 0;
  FDM.qJolly.Close;
  //                                         0          1          2
  FDM.qJolly.SQL.Text := Format('select sum(amount), sum(tax), sum(total) from invoice_Line where invoice_id = %d;', [FID]);
  FDM.qJolly.Open;
  if not(FDM.qJolly.Eof) then begin
    eTotal := FDM.qJolly.Fields[2].AsFloat;
    eTax := FDM.qJolly.Fields[1].AsFloat;
    eAmount := FDM.qJolly.Fields[0].AsFloat;
  end;
  FDM.qJolly.Close;

  (FBaseRec.FieldByName['amount'].UI as TNumberBox).Value := eAmount;
  FBaseRec.FieldByName['amount'].Value := (FBaseRec.FieldByName['amount'].UI as TNumberBox).Text;
  (FBaseRec.FieldByName['tax'].UI as TNumberBox).Value := eTax;
  FBaseRec.FieldByName['tax'].Value := (FBaseRec.FieldByName['tax'].UI as TNumberBox).Text;
  (FBaseRec.FieldByName['total'].UI as TNumberBox).Value := eTotal;
  FBaseRec.FieldByName['total'].Value := (FBaseRec.FieldByName['total'].UI as TNumberBox).Text;
  FBaseRec.Save(maEdit, nil);

end;

end.
