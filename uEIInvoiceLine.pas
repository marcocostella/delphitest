unit uEIInvoiceLine;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Dialogs, uEditInsert, FMX.Effects,
  FMX.Objects, FMX.Edit, uDM, Data.Win.ADODB;

type
  // inherited from the EditInsert FORM
  TfEIInvoiceLine = class(TfEditInsert)
  private
    { Private declarations }
    FInvoiceRec: TBaseRecord; // invoice record
  protected
    { Protected declarations }
    procedure CH_AfterSetUI; override;
    procedure EditsChange(Sender: TObject); override;
    procedure OnCalcFields(ABaseRec: TBaseRecord; AAdoQuery: TAdoQuery); override;
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

{ TfEIInvoiceLine }

procedure TfEIInvoiceLine.CH_AfterSetUI;
begin
  inherited;

  // in a production framework all this UI logic will be saved in
  // the model indfo

  FDM.UIService.SetFieldReadOnly(FBaseRec.FieldByName['invoice_id']);
  FDM.UIService.SetFieldReadOnly(FBaseRec.FieldByName['amount']);
  FDM.UIService.SetFieldReadOnly(FBaseRec.FieldByName['tax']);
  FDM.UIService.SetFieldReadOnly(FBaseRec.FieldByName['total']);

  if (FModAction = maInsert) then begin
    FBaseRec.FieldByName['quantity'].SetExtendedVal(1);
    FBaseRec.FieldByName['invoice_id'].UI.Text := FMasterRec.FieldByName['id'].Value;
  end;

  if (FMasterRec.FieldByName['status'].Value <> 'draft') then begin
    pEdit.Enabled := False;
    imgConfirm.Visible := False;
  end;

end;

procedure TfEIInvoiceLine.EditsChange(Sender: TObject);
var
  oBField: TBaseField;
  iTmp: Integer;
  eTmp2,
  eTmp: Extended;
  sTmp: string;
begin
  inherited;
  // this event is call all the time a value of one edit change
  // because this is a test framework i'm not going to do particular checks

  if (Sender is TNumberBox)
  and (((Sender as TNumberBox).Name = 'equantity') or ((Sender as TNumberBox).Name = 'eprice')) then begin

    // calc the tax of the customer
    eTmp2 := 0;
    oBField := FMasterRec.FieldByName['customer_id'];
    iTmp := oBField.GetComboKey;
    if (iTmp > -1) then begin
      FDM.qJolly.Close;
      FDM.qJolly.SQL.Text := Format('select tax from customer where id = %d', [iTmp]);
      FDM.qJolly.Open;
      if not(FDM.qJolly.Eof) then begin
        eTmp2 := FDM.qJolly.Fields[0].AsFloat;
      end;
      FDM.qJolly.Close;
    end;

    // amount total
    eTmp := FBaseRec.FieldByName['quantity'].GetExtendedVal * FBaseRec.FieldByName['price'].GetExtendedVal;
    FBaseRec.FieldByName['amount'].SetExtendedVal(eTmp);
    // tax total
    if (eTmp2 <> 0) then begin
      eTmp2 := eTmp / 100 * eTmp2
    end else begin
      eTmp2 := 0;
    end;
    FBaseRec.FieldByName['tax'].SetExtendedVal(eTmp2);
    // total invoice line
    FBaseRec.FieldByName['total'].SetExtendedVal(eTmp + eTmp2);
  end else if (Sender is TComboEdit) and ((Sender as TComboEdit).Name = 'eproduct_id') then begin
    oBField := FBaseRec.FieldByName['product_id'];
    if Assigned(oBField) then begin
      // get the price and product description of the product
      eTmp := 0;
      iTmp := oBField.GetComboKey;
      if (iTmp > -1) then begin
        FDM.qJolly.Close;
        FDM.qJolly.SQL.Text := Format('select sale_price, name from product where id = %d', [iTmp]);
        FDM.qJolly.Open;
        if not(FDM.qJolly.Eof) then begin
          eTmp := FDM.qJolly.Fields[0].AsFloat;
          sTmp := FDM.qJolly.Fields[1].AsString;
        end;
        FDM.qJolly.Close;
      end;

      // assign the price of the product
      FBaseRec.FieldByName['price'].SetExtendedVal(eTmp);
      FBaseRec.FieldByName['description'].UI.Text := sTmp;
    end;
  end;

end;

procedure TfEIInvoiceLine.OnCalcFields(ABaseRec: TBaseRecord; AAdoQuery: TAdoQuery);
begin
  inherited;
  AAdoQuery.FieldByName('invoice_id').AsString := FMasterRec.FieldByName['id'].Value;
end;

end.


